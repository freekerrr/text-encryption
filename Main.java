﻿import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    final static int secretKey = 1232;

    public static void main(String[] args) throws IOException {

        String text = "Я вас любил. Любовь еще (возможно,\n" +
                "что просто боль) сверлит мои мозги,\n" +
                "Все разлетелось к черту, на куски.";

        while (true) {
            System.out.flush();
            System.out.println("Выберите вариант шифрования: \n" +
                    "1. Шифрование с помощью \"XOR\"\n" +
                    "2. Шифрование с помощью секретного ключа\n" +
                    "3. Шифрование с помощью собственного ключа\n" +
                    "---Введите что либо другое для выхода");
            encryption(text);
        }
    }

    private static void encryption(String text) throws IOException {
        InputStreamReader read = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(read);
        String s = in.readLine();
        int number = 0;
        try {
            number = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            System.exit(0);
        }
        StringBuilder builder = new StringBuilder();
        char a;
        switch (number) {
            case 1:
                for (int i = 0; i < text.length(); i++) {
                    a = (char) ((int) text.charAt(i) ^ secretKey);
                    builder.append(a);
                }
                printEncryptionText(builder.toString());
                break;
            case 2:
                for (int i = 0; i < text.length(); i++) {
                    a = (char) ((int) text.charAt(i) + secretKey);
                    builder.append(a);
                }
                printEncryptionText(builder.toString());
                break;
            case 3:
                System.out.println("Введите ключ для шифрования (цифровой): ");
                number = Integer.parseInt(in.readLine());
                for (int i = 0; i < text.length(); i++) {
                    a = (char) ((int) text.charAt(i) + number);
                    builder.append(a);
                }
                printEncryptionText(builder.toString());
                break;
            default:
                System.exit(0);
        }
    }

    private static void printEncryptionText(String text) {
        System.out.println("Зашифрованный текст: \n" + text + "\n");
    }
}